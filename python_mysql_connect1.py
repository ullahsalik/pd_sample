import mysql.connector
from mysql.connector import Error

def connect():
    """ Connect to MySQL database """
    try:
        conn = mysql.connector.connect(host='18.234.255.159', database='new', user='root', password='redhat')
        if conn.is_connected():
            print('Connected to MySQL database')
        conn.close()
    except Error as e:
        print(e)

if __name__ == '__main__':
    connect()
